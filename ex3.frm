*comments
off stat;
function x,p;
symbols n;
local L1=x*p-p*x;
local L2=x^2*p-p*x^2;
local L3=x^3*p-p*x^3;
local L4=x^4*p-p*x^4;
local L5=x^5*p-p*x^5;
.sort 
repeat;
id x*p=p*x+i_;
endrepeat; 
print;
.end

